<?php

use yii\db\Migration;

/**
 * Handles the creation of table `users`.
 */
class m180427_081310_create_users_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
     	$tables = Yii::$app->db->schema->getTableNames();
		$dbType = $this->db->driverName;
		$tableOptions_mysql = "CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB";
		$tableOptions_mssql = "";
		$tableOptions_pgsql = "";
		$tableOptions_sqlite = "";
		/* MYSQL */
		if (!in_array('users', $tables))  { 
			if ($dbType == "mysql") {
				$this->createTable('{{%users}}', [
					'id' => 'INT(11) NOT NULL AUTO_INCREMENT',
					0 => 'PRIMARY KEY (`id`)',
					'fio' => 'TEXT NULL',
					'login' => 'TEXT NULL',
					'password' => 'TEXT NULL',
					'created_at' => 'DATETIME NULL',
				], $tableOptions_mysql);
			}
		}
		 
		 
		$this->execute('SET foreign_key_checks = 0');
		$this->insert('{{%users}}',['id'=>'1','fio'=>'admin','login'=>'admin','password'=>'admin','created_at'=>'2018-04-27 00:00:00']);
		$this->insert('{{%users}}',['id'=>'2','fio'=>'test123','login'=>'test123','password'=>'test123','created_at'=>'2018-04-27 10:30:47']);
		$this->execute('SET foreign_key_checks = 1;');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
     	$this->execute('SET foreign_key_checks = 0');
		$this->execute('DROP TABLE IF EXISTS `users`');
		$this->execute('SET foreign_key_checks = 1;');
    }
}
