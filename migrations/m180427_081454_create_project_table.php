<?php

use yii\db\Migration;

/**
 * Handles the creation of table `project`.
 */
class m180427_081454_create_project_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tables = Yii::$app->db->schema->getTableNames();
        $dbType = $this->db->driverName;
        $tableOptions_mysql = "CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB";
        $tableOptions_mssql = "";
        $tableOptions_pgsql = "";
        $tableOptions_sqlite = "";
        /* MYSQL */
        if (!in_array('project', $tables))  { 
            if ($dbType == "mysql") {
                $this->createTable('{{%project}}', [
                    'id' => 'INT(11) NOT NULL AUTO_INCREMENT',
                    0 => 'PRIMARY KEY (`id`)',
                    'name' => 'TEXT NULL',
                    'price' => 'DECIMAL(10,2) NULL',
                    'user_id' => 'INT(11) NULL',
                    'start_at' => 'DATETIME NULL',
                    'end_at' => 'DATETIME NULL',
                    'created_at' => 'DATETIME NULL',
                ], $tableOptions_mysql);
            }
        }
 
 
        $this->createIndex('idx_user_id_4488_00','project','user_id',0);
         
        $this->execute('SET foreign_key_checks = 0');
        $this->insert('{{%project}}',['id'=>'2','name'=>'Test project','price'=>'1000.00','user_id'=>'1','start_at'=>'2018-04-28 00:00:00','end_at'=>'2018-04-30 00:00:00','created_at'=>'2018-04-27 00:00:00']);
        $this->insert('{{%project}}',['id'=>'3','name'=>'Test project 2','price'=>'1000.00','user_id'=>'1','start_at'=>'2018-05-01 00:00:00','end_at'=>'2018-05-05 00:00:00','created_at'=>'2018-04-27 00:00:00']);
        $this->insert('{{%project}}',['id'=>'4','name'=>'Test project 3','price'=>'1000.00','user_id'=>'1','start_at'=>'2018-05-02 00:00:00','end_at'=>'2018-05-05 00:00:00','created_at'=>'2018-04-27 00:00:00']);
        $this->execute('SET foreign_key_checks = 1;');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->execute('SET foreign_key_checks = 0');
        $this->execute('DROP TABLE IF EXISTS `project`');
        $this->execute('SET foreign_key_checks = 1;');
    }
}
