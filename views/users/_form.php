<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Users */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="users-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'fio')->textInput(['class' => 'form-control']) ?>

    <?= $form->field($model, 'login')->textInput(['class' => 'form-control']) ?>

    <?= $form->field($model, 'password')->textInput(['class' => 'form-control']) ?>

    <?php /*= $form->field($model, 'created_at')->textInput() */?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
