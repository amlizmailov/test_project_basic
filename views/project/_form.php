<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\Users;
use dosamigos\datepicker\DatePicker;

/* @var $this yii\web\View */
/* @var $model app\models\Project */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="project-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'name')->textInput(['class' => 'form-control']) ?>

    <?= $form->field($model, 'price')->textInput(['maxlength' => true]) ?>

    <?php /*= $form->field($model, 'user_id')->textInput()*/ ?>

    <?php echo $form->field($model, 'user_id')->dropDownList(\yii\helpers\ArrayHelper::map(
        Users::find()->orderBy(['id' => SORT_ASC])->all(),
        'id',
        'fio'
    ), ['prompt'=>'']) ?>

    <label class="control-label" for="project-user_id">Date start</label>
    <?= DatePicker::widget([
        'model' => $model,
        'attribute' => 'start_at',
        'template' => '{addon}{input}',
        'clientOptions' => [
            'autoclose' => true,
            'format' => 'yyyy-mm-dd'
        ]
    ]);?>

    <label class="control-label" for="project-user_id">End start</label>
    <?= DatePicker::widget([
        'model' => $model,
        'attribute' => 'end_at',
        'template' => '{addon}{input}',
        'clientOptions' => [
            'autoclose' => true,
            'format' => 'yyyy-mm-dd'
        ]
    ]);?>

    <?php /*= $form->field($model, 'created_at')->textInput()*/ ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
